import Chroma from 'chroma-js';

function toLinear(c: number) {
	if (c <= 0.04045) return c / 12.92;
	else return Math.pow((c + 0.055) / 1.055, 2.4);
}

function fromLinear(c: number) {
	if (c <= 0.0031308) return 12.92 * c;
	else return 1.055 * Math.pow(c, 1 / 2.4) - 0.055;
}

function linearAverage(c1: number, c2: number, f: number) {
	return fromLinear(toLinear(c1) * (1-f) + toLinear(c2) * f)
}

export default function scale(c1: string, c2: string) {
	return (f: number) => {
		const [r1, g1, b1] = Chroma(c1).rgb();
		const [r2, g2, b2] = Chroma(c2).rgb();
		return Chroma(
			linearAverage(r1, r2, f),
			linearAverage(g1, g2, f),
			linearAverage(b1, b2, f),
			'rgb',
		);
	};
}
