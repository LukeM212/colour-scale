import React from 'react';
import Chroma from 'chroma-js';
import Canvas from './Canvas';

type ScaleProps = {f: (ratio: number) => Chroma.Color, width: number, height: number};
export default class Scale extends React.Component<ScaleProps> {
	render() {
		const {f, width, height} = this.props;
		
		return <Canvas
			draw={(ctx: CanvasRenderingContext2D) => {
				for (let x = 0; x < width; x++) {
					ctx.fillStyle = f(x / (width - 1)).css();
					ctx.fillRect(x, 0, 1, height);
				}
			}}
			width={this.props.width}
			height={this.props.height}
		/>;
	}
}
