import React from 'react';

type CanvasProps = {draw: (ctx: CanvasRenderingContext2D) => void, width: number, height: number};
export default function Canvas({draw, width, height}: CanvasProps) {
	const canvasRef = React.useRef<HTMLCanvasElement>(null);
	
	React.useEffect(() => {
		const canvas = canvasRef.current;
		if (canvas === null) return;
		const ctx = canvas.getContext('2d');
		if (ctx === null) return;
		draw(ctx);
	}, [draw, width, height]);
	
	return <canvas ref={canvasRef} width={width} height={height} />;
}
