import React from 'react';
import { SketchPicker } from 'react-color';
import Scale from './Scale';
import Chroma from 'chroma-js';
import lrgb2 from './lrgb2';
import './App.css';

type InterpolationMode = chroma.InterpolationMode | 'lrgb2';

const scaleModes: [InterpolationMode, string][] = [
	['rgb', "sRGB"],
	['lrgb', "Crude linear RGB"],
	['lrgb2', "True linear RGB"],
	['hsl', "HSL"],
	['hsv', "HSV"],
	['hsi', "HSI"],
	['lch', "Lch"],
	['lab', "Lab"],
];

function interpolate(c1: string, c2: string, m: InterpolationMode) {
	if (m === 'lrgb2') return lrgb2(c1, c2);
	else return Chroma.scale([c1, c2]).mode(m);
}

export default function App() {
	const [colour1, setColour1] = React.useState("#FF0000");
	const [colour2, setColour2] = React.useState("#00FF00");
	
	return (
		<div>{/* Page */}
			<div>{/* Content */}
				<SketchPicker color={colour1} disableAlpha={true} onChangeComplete={c => setColour1(c.hex)} />
				<SketchPicker color={colour2} disableAlpha={true} onChangeComplete={c => setColour2(c.hex)} />
				<br />
				{scaleModes.map(([mode, name]) => <div>
					<Scale f={interpolate(colour1, colour2, mode)} width={256} height={32} />
					{name}
				</div>)}
			</div>
		</div>
	);
}
